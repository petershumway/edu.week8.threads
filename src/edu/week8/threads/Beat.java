package edu.week8.threads;

public class Beat implements Runnable{

    private String lyrics;
    private int timing;
    private int repeats;
    private String intro;
    private int delay;

    //Constructor method
    public Beat(int timing, String lyrics, String intro, int repeats, int delay) {
        this.lyrics = lyrics;
        this.timing = timing;
        this.intro = intro;
        this.repeats = repeats;
        this.delay = delay;
    }

    // the run method is used when the class object is told to execute.
    public void run() {

        // initial delay for each part of the beat. try/catch is used for error handling
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            System.out.println("The system is literally down! Something is wrong...");
        }

        // introduce the new beat
        System.out.println(this.intro);
        System.out.println();

        // print out the beat lyric, sleep for the correct time between beats, try/catch is used for error handling
        for (int i = 0; i < repeats; i++) {
            System.out.println(lyrics);
            BeatCount.increase();
            try {
                Thread.sleep(timing);

            } catch (InterruptedException e) {
                System.out.println("The system is literally down! Something is wrong...");
            }
        }

    }
}
