package edu.week8.threads;

import java.util.concurrent.atomic.AtomicInteger;

public class BeatCount {

    // I made these methods and member variable static because they needed to be accessed by multiple object instances
    // of classes at the same time. I wasnt sure of a better way to do this.

    // Create an atomic integer to total the count of beats during the run of the program
    private static AtomicInteger count = new AtomicInteger();

    // method for increasing the integer so each thread can add without interrupting each other
    public static void increase() {
        count.incrementAndGet();
    }

    // method for getting the count total
    public static int getCount() {
        return count.get();
    }

}
