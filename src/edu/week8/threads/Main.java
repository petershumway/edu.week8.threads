package edu.week8.threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {

        // introduction
        System.out.println("This program simulates the Techno song made by Strongbad in the Homestar runner web " +
                "comic. It uses threads and executables in order to do it. \nYou can check out the song here " +
                "https://www.youtube.com/watch?v=JwZwkk7q25I");
        System.out.println();

        // create the executor with 4 threads (one for each beat in the techno song)
        ExecutorService myTechno = Executors.newFixedThreadPool(4);

        // setting the intro values
        String intro1 = "I heard a techno song one time that went like...";
        String intro2 = "\nand then this other part came in, and it was like...";
        String intro3 = "\nand then there's always some high pitched noise, y'know? Or like a siren that's like...";
        String intro4 = "\nAnd of course they have to put in the obligatory old movie quote from some sci-fi movie. " +
                "It's like...";

        // for each beat we create it will run on its own thread simultaneously

        // beat one needs to be twice a second for about 30 seconds
        Beat beat1 = new Beat(500, "Do", intro1, 60, 0);

        // beat two needs to start 4 seconds later and be once a second for about 22 seconds
        Beat beat2 = new Beat(1000, "Doodle-la Do\nDoodle-la Do", intro2, 22, 4000);

        // beat three needs to start 8 seconds later and be twice a second for about 18 seconds
        Beat beat3 = new Beat(500, "Do Doot Do Do Doodle", intro3, 36, 8000);

        // beat four needs to start 12 seconds later and be once a second for about 15 seconds.
        Beat beat4 = new Beat(1000, "The system is down!", intro4, 15, 12000);

        // execute each beat which will run simultaneously
        myTechno.execute(beat1);
        myTechno.execute(beat2);
        myTechno.execute(beat3);
        myTechno.execute(beat4);

        // shut down the executor service so that it doesnt run indefinitely
        myTechno.shutdown();

        //Goodbye message done by a different thread.
        GoodbyeThread goodbye = new GoodbyeThread();
        goodbye.start();

    }
}
