package edu.week8.threads;

public class GoodbyeThread extends Thread{

    public void run() {
        // this thread delays till the program is over then says goodbye. try/catch is used for error handling
        try {
            Thread.sleep(31000);
        } catch (InterruptedException e) {
            System.out.println("The system is literally down! Something is wrong...");
        }
        System.out.println();

        // display a goodbye message plus the total of beats in the song.
        System.out.println("Thanks for jammin! This techno song had " + BeatCount.getCount() + " beats. Goodbye!");
    }
}
